var gallery_index = 2;

var gallery = [
  "../media/images/logan/1.jpg",
  "../media/images/logan/2.jpg",
  "../media/images/logan/3.jpg",
  "../media/images/logan/4.jpg",
  "../media/images/logan/5.jpg",
  "../media/images/logan/6.jpg",
  "../media/images/logan/7.jpg",
  "../media/images/logan/8.jpg",
  "../media/images/logan/9.jpg",
  "../media/images/logan/10.jpg",
  "../media/images/logan/11.jpg",
];

function view_previous() {
  if(gallery_index <= 1) {
    gallery_index = gallery.length;
  }
  else {
    gallery_index--;
  };

  document
    .getElementsByClassName("gallery")[0]
      .style
        .setProperty("--next_pic", "url(../media/images/logan/" + gallery_index + ".jpg) no-repeat center center fixed");
    
  document
    .getElementsByTagName("img")[0]
      .src = "../media/images/logan/" + gallery_index + ".jpg";
};

function view_next() {
  if(gallery_index >= gallery.length) {
    gallery_index = 1;
  }
  else {
    gallery_index++;
  };

  document
    .getElementsByClassName("gallery")[0]
      .style
        .setProperty("--next_pic", "url(../media/images/logan/" + gallery_index + ".jpg) no-repeat center center fixed");
  document
    .getElementsByTagName("img")[0]
      .src = "../media/images/logan/" + gallery_index + ".jpg";
};

function view_random() {
  gallery_index = random_image(1, gallery.length);
  view_previous();
}

function random_image(min, max) {
  return (Math.random() * (max - min + 1)) << 0;
}

document
  .addEventListener("DOMContentLoaded", function() {
    gallery_index = random_image(1, gallery.length);
    view_previous();
}, false)