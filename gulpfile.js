/*
Initialise and assign Gulp.js modules to variables.
Install these with 'npm i -D <name>' to save dev dependencies in package.json.
*/
var autoprefixer  = require('autoprefixer');
var browserSync   = require('browser-sync');
var concat        = require('gulp-concat');
var filter        = require('gulp-filter');
var gulp          = require('gulp');
var minifyCSS     = require('cssnano');
var minifyHTML    = require('gulp-htmlmin');
var minifyIMG     = require('gulp-imagemin');
var minifyJS      = require('gulp-uglify');
var newer         = require('gulp-newer');
var postCSS       = require('gulp-postcss');
var pug           = require('gulp-pug');
var sass          = require('gulp-sass');
var sourcemaps    = require('gulp-sourcemaps');

/*
Temporary variables for handling interim build operations.
*/
var browserSyncSHA  = 'sha256-7+J4BBpfP6BrGwq4jUTZH0MyBpT74yWhCBZ2TproeZY=';

/*
List of directory references.
*/
var dir = {
  start_page:         '/',
  public_pages:       './public/',
  public_images:      './public/media/images/',
  public_styles:      './public/styles/',
  public_scripts:     './public/scripts/',
  source_pages:       './source/pages/',
  source_pages_all:   './source/pages/**/*.{pug,md}',
  source_pages_pug:   './source/pages/**/*.pug',
  source_images_all:  './source/media/images/**/*.{png,jpg,svg,gif}',
  source_styles_all:  [
                      './source/styles/normalize.css',
                      './source/styles/layout.sass',
                      ],
  source_scripts_all: [
                      './source/scripts/background-controls.js',
                      ],
};

/*
Compile Pug to HTML.
1.  Assign base directory of pages to enable includes and extends for Pug
    compilation with respect to relative/absolute file paths.
2.  Ignore partials prefixed with '_' in file or folder name during the build
    process to avoid littering unnecessary HTML into distribution.
3.  Minify HTML after Pug compilation due to Markdown interpolation.
*/
gulp.task('build_pages', function () {
  return gulp
  .src(dir.source_pages_pug)
  .pipe(pug({ basedir: dir.source_pages })) /* 1 */
  .pipe(filter(function (file) { /* 2 */
    return !/\/_/.test(file.path) && !/^_/.test(file.relative);
  }))
  .pipe(minifyHTML({ collapseWhitespace: true, removeComments: true })) /* 3 */
  .pipe(gulp.dest(dir.public_pages));
});

/*
Minify images for distribution.
1.  Only pass through files that are newer than the destination ones.
2.  Minify images for web rendering.
3.  Trigger browser to inject images upon their minification.
*/
gulp.task('build_images', function () {
  return gulp
  .src(dir.source_images_all)
  .pipe(newer(dir.public_images)) /* 1 */
  .pipe(minifyIMG()) /* 2 */
  .pipe(gulp.dest(dir.public_images))
  .pipe(browserSync.reload({ stream: true })); /* 3 */
});

/*
Compile SASS to CSS with common vendor prefixes.
1.  Output sourcemaps in a separate file.
2.  Compile SASS to minified CSS and log potential errors.
3.  Amend vendor prefixes using default configuration of browser usage: > 0.5%,
    last 2 versions, Firefox ESR, not dead.
4.  Suffix '.min' to CSS file to indicate it is minified to the server.
5.  Trigger browser to inject CSS upon compilation of SASS.
*/
gulp.task('build_styles', function () {
  return gulp
  .src(dir.source_styles_all)
  .pipe(sourcemaps.init()) /* 1 */
  .pipe(sass()) /* 2 */
  .on('error', sass.logError) /* 2 */
  .pipe(concat('main.min.css')) /* 4 */
  .pipe(postCSS([autoprefixer, minifyCSS])) /* 3 */
  .pipe(sourcemaps.write('./')) /* 1 */
  .pipe(gulp.dest(dir.public_styles))
  .pipe(browserSync.reload({ stream: true })); /* 5 */
});

/*
Minify and concatenate JavaScript.
1.  Output sourcemaps in a separate file.
2.  Concatenate all files into one new one.
3.  Minify JavaScript with default configuration.
    Options: https://github.com/mishoo/UglifyJS2#minify-options
*/
gulp.task('build_scripts', function () {
  return gulp
  .src(dir.source_scripts_all)
  .pipe(sourcemaps.init()) /* 1 */
  .pipe(concat('main.min.js')) /* 2 */
  .pipe(minifyJS()) /* 3 */
  .pipe(sourcemaps.write('./')) /* 1 */
  .pipe(gulp.dest(dir.public_scripts));
});

/*
Run build processes then watch for changes to update or reload the browser.
1.  Initialise local server at default location: http://localhost:3000/
*/
gulp.task('build_all', ['build_pages', 'build_styles', 'build_images', 'build_scripts'], function () {
  browserSync({
    server: dir.public_pages,
    startPath: dir.start_page,
    snippetOptions: {
      ignorePaths: '',
    },
    rewriteRules: [
      {
        match: "script-src 'self';",
        replace: "script-src 'self' '" + browserSyncSHA + "'; connect-src ws: 'self';",
      },
    ],
  }); /* 1 */
  gulp.watch(dir.source_pages_all, ['build_pages', browserSync.reload]);
  gulp.watch(dir.source_styles_all, ['build_styles']);
  gulp.watch(dir.source_images_all, ['build_images', browserSync.reload]);
  gulp.watch(dir.source_scripts_all, ['build_scripts', browserSync.reload]);
});
